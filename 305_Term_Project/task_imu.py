''' @file           task_imu.py
    @brief          creates IMU object and sets up IMU frequency
    @details        handles IMU data collection to be printed through task_user
                    Source code for our file can be found here:
                    https://bitbucket.org/wimberle/me305_labs/src/master/305_Term_Project/task_imu.py

    @author         Philip Pang
    @author         Matthew Wimberley
    @date           October 26, 2021
'''

import utime

class IMUTasks:
    '''@brief       instantiates self object of IMU data
       @details     handles BNO055 data collection and calibration status to be 
                    called from task_user
    '''

    def __init__(self, freq_imu, BNO055, balancing, collect, imu_state):

        '''@brief   initializes all IMU data collection and frequency it does so
           @param   frequency at which IMU runs
           @param   IMU system
           @param   balancing flag
           @param   collection flag
           @param   IMU calibration state
           '''

        self.freq_imu = freq_imu
        self.period = 1 / freq_imu * 1000000  # in us (microseconds)
        self.next_time = 0

        self.imu = BNO055

        self.balancing = balancing
        self.collect = collect
        self.imu_state = imu_state # (theta_y [roll], theta_x [pitch], omega_y, omega_x)


    def run(self):
        '''@brief       runs IMU task 
           @details     only if IMU data collection is happening will the angles
                        and velocities be written
        '''
           
        # @brief   resets encoder positions then updates it
        if (utime.ticks_us() >= self.next_time):

            self.next_time = utime.ticks_us() + self.period

            angles = self.imu.read_angles()         # heading, roll, pitch
            velocities = self.imu.read_velocity()   # roll, pitch, heading
            self.imu_state.write((angles[1], angles[2], velocities[0] , velocities[1]))
                                # (theta_y [roll], theta_x [pitch], omega_y, omega_x)

