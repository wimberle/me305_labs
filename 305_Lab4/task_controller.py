''' @file           task_controller.py
    @brief          instantiates all controller variables to optimize PID
    @details        handles unit conversion of ticks and controls speed error

    @author         Philip Pang
    @author         Matthew Wimberley
    @date           November 4, 2021
'''


import utime

class ControllerTasks:
    def __init__(self, freq, step_flag, closedLoop, duty, delta, alg, startTime, stop, target, kp = 0, ki = 0, kd = 0):
        
        ''' @brief      instantiates gains, target speed, and flag
            @details    period is as small as we can receive data
        '''
        
        self.freq = freq
        self.step_flag = step_flag
        self.closedLoop = closedLoop
        self.duty = duty
        self.delta = delta
        self.alg = alg
        self.startTime = startTime
        self.stop = stop
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.target = target                # (rad/s)
        self.period = 1 / freq * 1000000    # in us (microseconds)
        self.next_time = 0

    def run(self):
                
        ''' @brief      runs closed control loop
            @details    converts units to radians and updates all gain and speed values
        '''
        
        if (utime.ticks_us() >= self.next_time):
            self.next_time = utime.ticks_us() + self.period
            
            if self.step_flag.read():
                elapsed_time = utime.ticks_diff(utime.ticks_us(), self.startTime.read())
                if (elapsed_time < 10000000):  # in microseconds
                    current_speed = self.delta.read() * 2.0 * 3.1415926535 / 4000 * self.freq
                    self.duty.write(self.closedLoop.update(self.alg.read(), self.target.read(), current_speed,
                                                           self.kp.read(), self.ki.read(), self.kd.read()))
                else:
                    self.stop.write(1)
