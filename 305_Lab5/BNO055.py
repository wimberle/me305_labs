import pyb
import utime
from pyb import I2C
import binascii
import array
import struct


class BNO055:

    def __init__(self, i2c):
        self.i2c = i2c
        self.coeff = bytearray(22)
        self.angles = bytearray(6)
        self.vels = bytearray(6)
        self.status = bytearray(1)

    def set_units(self, units):
        mode = self.i2c.mem_read(1, 40, 0x3D)
        self.i2c.mem_write(0, 40, 0x3D)
        self.i2c.mem_write(units, 40, 0x3B)
        self.i2c.mem_write(mode, 40, 0x3D)

    def change_mode(self, mode):
        self.i2c.mem_write(mode, 40, 0x3D)

    def calib_status(self):
        self.i2c.mem_read(self.status, 40, 0x35)
        return self.status[0]

    def read_coeff(self):
        self.i2c.mem_read(self.coeff, 40, 0x55)

    def write_coeff(self):
        self.i2c.mem_write(self.coeff, 40, 0x55)

    def read_angles(self):
        self.i2c.mem_read(self.angles, 40, 0x1A)
        ang = [0,0,0]
        ang[0] = self.angles[0] | self.angles[1] << 8  # EUL_Heading
        ang[1] = self.angles[2] | self.angles[3] << 8  # EUL_Roll
        ang[2] = self.angles[4] | self.angles[5] << 8  # EUL_Pitch
        for i in range(3):
            if ang[i] > 32767:
                ang[i] -= 65536
        print('Euler Angles - (rad)\n'
              'Heading:{:}\tRoll:{:}\tPitch:{:}'.format(ang[0]/900, ang[1]/900, ang[2]/900))

    def read_velocity(self):
        self.i2c.mem_read(self.vels, 40, 0x14)
        vel = [0,0,0]
        vel[0] = self.vels[0] | self.vels[1] << 8  # EUL_Heading
        vel[1] = self.vels[2] | self.vels[3] << 8  # EUL_Roll
        vel[2] = self.vels[4] | self.vels[5] << 8  # EUL_Pitch
        for i in range(3):
            if vel[i] > 32767:
                vel[i] -= 65536
        print('Angular Velocities - (rad/s)\n'
              'X:{:}\tY:{:}\tZ:{:}'.format(vel[0]/900, vel[1]/900, vel[2]/900))

    def print_calib(self):
            out = [False, False, False, False]
            SYS = 0b11000000
            GYR = 0b00110000
            ACC = 0b00001100
            MAG = 0b00000011
            if (self.status[0] & SYS) == SYS:
                out[0] = True
            if (self.status[0] & GYR) == GYR:
                out[1] = True
            if (self.status[0] & ACC) == ACC:
                out[2] = True
            if (self.status[0] & MAG) == MAG:
                out[3] = True
            return out



if __name__ == '__main__':

    i2c = I2C(1, I2C.MASTER)
    imu = BNO055(i2c)
    imu.change_mode(12)
    imu.set_units(6)

    print("CALIBRATING...")
    print("[SYS,  GYR,  ACC,  MAG]")

    while True:
        binary = imu.calib_status()
        print(str(imu.print_calib())+ "\t"+ str(binary))
        if(binary == 0b11111111):
            imu.read_coeff()
            print("CALIBRATION COMPLETE!")
            break
        utime.sleep(2)

    while True:
        imu.read_angles()
        imu.read_velocity()
        utime.sleep(1)
