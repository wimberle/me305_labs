'''@file ME305_Lab0.py 
    @brief enter index return fibonacci number'''

def fib(idx):
        
        f, n = 0, 1
        for idx in range(idx): #counts up fib sequence idx times
            f, n = n, f + n
        return f
    
if __name__ == '__main__':

    while True: 
    
        idx = input("Enter a whole number index: ")
        
        try:
            idx = int(idx) #make sure an integer
                
        except:
            print("\r\nPlease enter a whole number!")
            continue
        
        if idx < 0 or idx != int(idx): #make sure nonnegative 
            print("\nPlease enter a whole number")
            

        else:
            print('Fibonacci number at '
                     'index {:} is {:}.'.format(idx,fib(idx))) 
                
        ask = str(input('Enter q to quit or any other '
                    'key to find more Fibonacci numbers  '))
        
        if ask == ('q'):
                break #breaks loop
        
        
        
    

   