# -*- coding: utf-8 -*-
"""
Created on Tue Oct  5 15:23:53 2021

@author: matts
"""

import pyb
import time
import EncoderTasks

task1 = EncoderTasks.Task_Example()
task2 = EncoderTasks.Task_Example()

tim4 = pyb.Timer(4, prescaler = 0, period = 65535)
ch1 = tim4.channel(1, pyb.Timer.ENC_AB, pin = pyb.Pin.board.PB6)
ch1 = tim4.channel(2, pyb.Timer.ENC_AB, pin = pyb.Pin.board.PB7)

# functions defined before main program


if __name__ == '__main__':
    

    while(True):
        try:
            task1.run()
            task2.run()
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')