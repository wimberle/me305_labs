""" @file       task_user
@brief      communicates user input with nucleo board
@details    does not need to import other files but will be instead called through main
@details    different inputs will run different commands

@author:    Philip Pang
@author:    Matthew Wimberley
"""
import pyb
import utime

class UserTasks:
    

    '''@brief    discerns user input
       @details  all UI focused here'''
    
    def __init__(self, freq, zero_pos, curr_pos1, delta1, curr_pos2, delta2, delta_queue1):
        '''@brief   initialized variables just like in task_encoder and main
        @param   all positional values related to both encoders'''
        

        self.zero_pos = zero_pos
        self.curr_pos1 = curr_pos1
        self.delta1 = delta1
        self.curr_pos2 = curr_pos2
        self.delta2 = delta2
        self.delta_queue = delta_queue1
        self.period = 1/freq * 1000000       # in us (microseconds)
        self.port = pyb.USB_VCP()
        self.state = 0
        self.startTime = 0
        self.next_time = utime.ticks_us()+ self.period

    def empty_queue(self, queue):

    
        '''@brief   resets queue
       @return  returns 0 value'''
       

        while queue.num_in() > 0:
            queue.get()
        return 0

    def print_queue(self, queue):

        
        '''@brief   retrieves and prints queue
       @return  returns 0 value'''
        

        while queue.num_in() > 0:
            print("E1: d = " + str(queue.get()))
        return 0

    def run(self):

        
        '''@brief   runs task user 
       @details constantly waiting for correct input from user to execute
       '''
        
        if (utime.ticks_us() >= self.next_time):
            
            '''@brief   if encoder time still running the rest will execute'''
    
            if (self.state == 0):
                '''@brief   state 0 gives instructions then proceeds to state 1'''
    

                print ("+---------------------------------------------------------------------------+\n"
                       "| Encoder Testing Interface - Matt/Philip                                   |\n"
                       "+---------------------------------------------------------------------------+\n"
                       "| Use the following commands:                                               |\n"
                       "|   z       Zero the position of encoder 1 and 2                            |\n"
                       "|   p       Print out the position of encoder 1 and 2                       |\n"
                       "|   d       Print out the delta for encoder 1 and 2                         |\n"
                       "|   g       Collect data from encoder 1 only for 30 seconds                 |\n"
                       "|   s       End data collection from \"g\" command immediately            |\n"
                       "|   h       Display this help message                                       |\n"
                       "+---------------------------------------------------------------------------+\n"
                       )

                self.state = 1

            elif (self.state == 1):
                
                '''@brief   waits for 6 commands listed above
                @param   one of 6 inputs
               @details if any of 6 commands is hit then appropriate command will follow
               '''
       
                # waiting for user input state
                if (self.port.any()):
                    
                    user_input = self.port.read(1).decode()
                    
                    if user_input == ('z'):
                        print("Zeroing Encoders\n")
                        self.zero_pos.write(True)
                        
                    elif user_input == ('p'):
                        print("E1: t = {:}, p = {:}".format(utime.ticks_ms(), self.curr_pos1.read()))
                        print("E2: t = {:}, p = {:}\n".format(utime.ticks_ms(), self.curr_pos2.read()))
                        
                    elif user_input == ('d'):
                        print("E1: t = {:}, d = {:}".format(utime.ticks_ms(), self.delta1.read()))
                        print("E2: t = {:}, d = {:}\n".format(utime.ticks_ms(), self.delta2.read()))
                        
                    elif user_input == ('g'):
                        print("---------------------------START DATA COLLECTION ----------------------------\n")
                        self.state = 2
                        self.startTime = utime.ticks_ms()
                        self.empty_queue(self.delta_queue)
                        
                    elif user_input == ('h'):
                        self.state = 0

            elif (self.state == 2):
                
                '''@brief   gathers data for 30 seconds
                @details put into separate state so that "s" key can be utilized'''
                
                # Gather delta for 30 seconds and print as list
                if((utime.ticks_ms() - self.startTime) < 30000):    # in milliseconds
                    self.delta_queue.put(self.delta1.read())
                    if (self.port.any()):           # CASE: valid input while in "g"
                        user_input = self.port.read(1).decode()
                        if user_input == ('s'):
                            self.state = 3          # CASE: "s" command was given
                        else:
                            self.state = 2          # CASE: there was input but not ("s")
                    else:
                        self.state = 2              # CASE: there was no input
                else:
                    self.state = 3                  # CASE: 30 seconds has elapsed

            elif (self.state == 3):
                # Print delta queue
                
                '''@brief   advances to next state after data collection is finished'''
    
                self.print_queue(self.delta_queue)
                print("----------------------------DATA COLLECTION DONE----------------------------\n")
                self.state = 1

            self.next_time += self.period

