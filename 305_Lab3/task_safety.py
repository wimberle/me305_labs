'''@file            task_safety.py
   @brief           enables motors if no fault is occuring and disables faulted motor
   @details         constantly updating and checking for fault flag
                    Source code found here: https://bitbucket.org/wimberle/me305_labs/src/master/305_Lab3/task_safety.py    
   @author          Philip Pang
   @author          Matthew Wimberley
   @date            October 30, 2021   
'''

import utime

class SafetyTasks:
    def __init__(self, freq, DRV8847, fault_flag, clr_fault, enable_motors):
        
        '''@brief       instantiates the motors and flags
           
        '''
        self.freq = freq
        self.DRV8847 = DRV8847
        self.fault_flag = fault_flag
        self.clr_fault = clr_fault
        self.enable_motors = enable_motors
        self.period = 1 / freq * 1000000  # in us (microseconds)
        self.next_time = utime.ticks_us() + self.period


    def run(self):
        '''@brief       prevents motor from harming itself
           @details     enables motor once fault is cleared and lowers flag
        '''
        
        if (utime.ticks_us() >= self.next_time):
            if self.clr_fault.read():
                self.DRV8847.enable()
                self.clr_fault.write(0)
                self.enable_motors.write(1)
                self.fault_flag.write(0)
            self.next_time += self.period
